import task1.Student;
import task1.StudentStorage;
import task2.Patient;
import task2.PatientStorage;
import task3.Abiturient;
import task3.AbiturientStorage;

public class Main {
    public static void main(String[] args) {
        test1();
        test2();
        test3();
    }

    public static void test1() {
        var storage = new StudentStorage();
        storage.addStudent(new Student("Amongus", 1954));
        storage.addStudent(new Student("AmongSuS", 1967));
        storage.addStudent(new Student("Petya", 2003));
        storage.addStudent(new Student("Vi", 2077));

        storage.printAllStudentsAfterFollowingYear(1967);
    }

    public static void test2() {
        var storage = new PatientStorage();
        storage.addPatient(new Patient("Amongus", "Shiza"));
        storage.addPatient(new Patient("AmongSuS", "League of Legends"));
        storage.addPatient(new Patient("Petya", "Shiza"));
        storage.addPatient(new Patient("Vi", "Minecraft"));

        storage.printAllPatientsWithDiagnosis("Shiza");
    }

    public static void test3() {
        var storage = new AbiturientStorage();
        storage.addAbiturient(new Abiturient("Vi"));
        storage.addAbiturient(new Abiturient("Petya"));
        storage.addAbiturient(new Abiturient("AmongSuS"));
        storage.addAbiturient(new Abiturient("Amongus"));

        storage.printInAlphabetOrder();
    }
}
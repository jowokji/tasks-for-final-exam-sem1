package task3;

public class Abiturient {
    private String lastName;

    public Abiturient(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return "{lastName: " + lastName + "}";
    }
}

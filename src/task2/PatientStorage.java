package task2;

import java.util.ArrayList;

public class PatientStorage {
    private ArrayList<Patient> inner;

    public PatientStorage() {
        inner = new ArrayList<Patient>();
    }

    public void addPatient(Patient patient) {
        inner.add(patient);
    }

    public void printAllPatientsWithDiagnosis(String diagnosis) {
        System.out.printf("All patients with dianosis \"%s\":\n", diagnosis);
        for (var patient : inner) {
            if (patient.getDiagnosis().equals(diagnosis)) {
                System.out.println(patient);
            }
        }
    }
}

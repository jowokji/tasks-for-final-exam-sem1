package task2;

public class Patient {
    private String name;
    private String diagnosis;

    public Patient(String name, String diagnosis) {
        this.name = name;
        this.diagnosis = diagnosis;
    }

    public String getName() {
        return name;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    @Override
    public String toString() {
        return String.format("{name: %s; diagnosis: %s}", name, diagnosis);
    }
}